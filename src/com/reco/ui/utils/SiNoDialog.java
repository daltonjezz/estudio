package com.reco.ui.utils;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SiNoDialog extends Window implements Button.ClickListener {

	private static final long serialVersionUID = 1L;

	Callback callback;
	Button si = new Button("Si", this);
	Button no = new Button("No", this);
	VerticalLayout contenido;

	public SiNoDialog(String caption, String question, Callback callback) {
		super(caption);
		this.contenido = new VerticalLayout();
		setModal(true);

		this.callback = callback;

		if (question != null) {
			contenido.addComponent(new Label(question));
		}

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(si);
		hl.addComponent(no);
		contenido.addComponent(hl);
		setContent(contenido);
		((VerticalLayout) getContent()).setComponentAlignment(hl, Alignment.BOTTOM_RIGHT);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (getParent() != null) {
			getParent().getUI().removeWindow(this);
		}
		callback.onDialogResult(event.getSource() == si);

	}

	public interface Callback {

		public void onDialogResult(boolean resultIsYes);
	}

}
