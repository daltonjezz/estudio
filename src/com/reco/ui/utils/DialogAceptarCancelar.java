package com.reco.ui.utils;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Window;

public class DialogAceptarCancelar extends Window implements Button.ClickListener {

	private static final long serialVersionUID = 1L;

	protected CssLayout contenido;
	protected Component component;
	protected Button bAceptar = new Button("Aceptar", this);
	protected Button bCancelar = new Button("Cancelar", this);

	public DialogAceptarCancelar(String caption, Component component) {
		super(caption);
		this.component = component;
		contenido = new CssLayout();
		// contenido.addStyleName("pantalla-operacion");

		setModal(true);
		setWidth("50%");

		if (component != null) {
			contenido.addComponent(component);
		}

		HorizontalLayout hl = new HorizontalLayout(bAceptar, bCancelar);
		contenido.addComponent(hl);
		// ((VerticalLayout) getContent()).setComponentAlignment(hl,
		// Alignment.BOTTOM_CENTER);
		setContent(contenido);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (getParent() != null) {
			getParent().getUI().removeWindow(this);
		}
		// callback.onDialogResult(event.getSource() == bAceptar);

	}

	public void habilitadBotones(Boolean estado) {
		this.bAceptar.setEnabled(estado);
	}

}
