package com.reco.app.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;

public class VistaMenu extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "MAIN";
	private OperationContainer contenedorOperaciones;

	public VistaMenu() {

		contenedorOperaciones = new OperationContainer();
		addComponent(doMenu());
		addComponent(contenedorOperaciones);
	}

	private CssLayout doMenu() {

		OperationFactory operationFactory = new OperationFactory(contenedorOperaciones);
		CssLayout layoutMenu = new CssLayout();

		MenuBar menu = new MenuBar();
		menu.addItem("Operacion 1", new AbrirOperacionCommand(
				new Operation("Concreta 1", "com.reco.pantallasConcretas.PantallaContreta1"), operationFactory));
		menu.addItem("Operacion 2", new AbrirOperacionCommand(
				new Operation("Concreta 2", "com.reco.pantallasConcretas.PantallaConcreta2"), operationFactory));
		layoutMenu.addComponent(menu);

		return layoutMenu;
	}

	private final class AbrirOperacionCommand implements Command {

		private static final long serialVersionUID = 1L;

		private Operation operation;
		private OperationFactory factory;

		public AbrirOperacionCommand(Operation operation, OperationFactory factory) {

			this.operation = operation;
			this.factory = factory;
		}

		@Override
		public void menuSelected(MenuItem selectedItem) {
			factory.abrirOperacion(operation);
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (VaadinSession.getCurrent().getAttribute("usuario") == null) {
			System.out.println("no hay usuario");
			getUI().getNavigator().navigateTo(VistaLogin.NAME);
		}

	}

}
