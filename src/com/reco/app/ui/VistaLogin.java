package com.reco.app.ui;

import com.reco.helper.BeanHelper;
import com.reco.usuarios.ControladorUsuarios;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class VistaLogin extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	public static final String NAME = "";

	ControladorUsuarios controladorUsuarios;
	TextField usuario;
	PasswordField password;

	public VistaLogin() {
		initUI();
	}

	private void initUI() {

		usuario = new TextField("Usuario");
		password = new PasswordField("Contraseña");
		controladorUsuarios = (ControladorUsuarios) BeanHelper.getBean("controladorUsuarios");

		Button button = new Button("Ingresar", new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String nombreUsuario = usuario.getValue();
				String contrasenia = password.getValue();

				if (controladorUsuarios.validaUsuario(nombreUsuario, contrasenia)) {
					getSession().setAttribute("usuario", usuario);
					Page.getCurrent().setUriFragment("!" + VistaMenu.NAME);
				} else {
					Notification.show("Usuario o contraseña invalidos", Type.ERROR_MESSAGE);
				}

			}
		});

		VerticalLayout fields = new VerticalLayout(usuario, password, button);
		fields.setSpacing(true);
		fields.setMargin(new MarginInfo(true, true, true, false));
		fields.setSizeUndefined();

		addComponent(fields);
		setComponentAlignment(fields, Alignment.MIDDLE_CENTER);
		setSizeFull();
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

}
