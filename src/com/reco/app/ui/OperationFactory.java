package com.reco.app.ui;

import java.io.Serializable;

public class OperationFactory implements Serializable {

	private static final long serialVersionUID = 1L;

	private OperationContainer container;

	public OperationFactory(OperationContainer contenedorOperacion) {
		container = contenedorOperacion;
	}

	public void abrirOperacion(Operation op) {
		IPantalla pantallaContreta = obtenerOperacion(op);
		container.abrirPantalla(pantallaContreta);
	}

	protected IPantalla obtenerOperacion(Operation operation) {
		try {
			return (IPantalla) Class.forName(operation.getClazz()).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
