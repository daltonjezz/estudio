package com.reco.app.ui;

import com.vaadin.ui.CssLayout;

public class PantallaConcreta extends CssLayout implements IPantalla {

	private static final long serialVersionUID = 1L;

	private String nombre;
	private IPantalla padre;
	private OperationContainer container;

	public PantallaConcreta(String nombre, IPantalla padre) {
		this.nombre = nombre;
		this.padre = padre;
	}

	public void initUI() {

	}

	@Override
	public String getNombre() {
		return nombre;
	}

	@Override
	public IPantalla getPadre() {
		return padre;
	}

	@Override
	public OperationContainer getContainer() {
		return container;
	}

	@Override
	public void setContainer(OperationContainer operationContainer) {
		this.container = operationContainer;

	}

}
