package com.reco.app.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.Page.UriFragmentChangedEvent;
import com.vaadin.server.Page.UriFragmentChangedListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@Theme("reco")
public class MainUI extends UI {

	private static final long serialVersionUID = 1L;

	Navigator navigator;

	@Override
	protected void init(VaadinRequest request) {
		getPage().setTitle("Aplicacion");
		navigator = new Navigator(this, this);

		navigator.addView(VistaLogin.NAME, VistaLogin.class);
		navigator.setErrorView(VistaLogin.class);
		//

		Page.getCurrent().addUriFragmentChangedListener(new UriFragmentChangedListener() {

			@Override
			public void uriFragmentChanged(UriFragmentChangedEvent event) {
				router(event.getUriFragment());

			}

		});

		router("");

	}

	private void router(String uriFragment) {
		if (getSession().getAttribute("usuario") != null) {
			navigator.addView(VistaMenu.NAME, VistaMenu.class);

			if (uriFragment.equals("!" + VistaMenu.NAME)) {
				navigator.navigateTo(VistaMenu.NAME);
			}
		} else {
			navigator.navigateTo(VistaLogin.NAME);
		}

	}

}