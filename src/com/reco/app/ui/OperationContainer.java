package com.reco.app.ui;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class OperationContainer extends Panel {

	private static final long serialVersionUID = 1L;

	private Label operationName;
	private IPantalla iPantalla;
	private VerticalLayout contenido;

	public OperationContainer() {
		setSizeUndefined();
		operationName = new Label("Operacion ");
		contenido = new VerticalLayout();
		setContent(contenido);
	}

	public void abrirPantalla(IPantalla pantalla) {
		iPantalla = pantalla;
		contenido.removeAllComponents();
		operationName.setValue(iPantalla.getNombre());
		operationName.setSizeUndefined();
		iPantalla.setSizeUndefined();
		contenido.addComponent(operationName);
		contenido.addComponent(iPantalla);

	}

}
