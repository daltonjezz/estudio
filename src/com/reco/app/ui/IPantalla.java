package com.reco.app.ui;

import com.vaadin.ui.Component;

public interface IPantalla extends Component {

	public String getNombre();

	public IPantalla getPadre();

	public OperationContainer getContainer();

	public void setContainer(OperationContainer operationContainer);

}
