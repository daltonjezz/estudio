package com.reco.app.ui;

import java.io.Serializable;

public class Operation implements Serializable {

	private static final long serialVersionUID = 1L;

	String operationName;
	String clazz;

	public Operation(String operationName, String clazz) {
		super();
		this.operationName = operationName;
		this.clazz = clazz;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

}
