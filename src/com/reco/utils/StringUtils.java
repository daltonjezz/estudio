package com.reco.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StringUtils {

	public static boolean esStringVacioONull(String string) {
		return (string == null || string.length() == 0);
	}

	public static String createCaptionByPropertyId(Object propertyId) {
		String name = propertyId.toString();
		if (name.length() > 0) {

			if (name.indexOf(' ') < 0 && name.charAt(0) == Character.toLowerCase(name.charAt(0))
					&& name.charAt(0) != Character.toUpperCase(name.charAt(0))) {
				StringBuffer stringBuffer = new StringBuffer();
				stringBuffer.append(Character.toUpperCase(name.charAt(0)));
				int i = 1;

				while (i < name.length()) {
					int j = i;
					for (; j < name.length(); j++) {
						char c = name.charAt(j);
						if (Character.toLowerCase(c) != c && Character.toUpperCase(c) == c) {
							break;
						}
					}
					if (j == name.length()) {
						stringBuffer.append(name.substring(i));
					} else {
						stringBuffer.append(name.substring(i, j));
						stringBuffer.append(" " + name.charAt(j));
					}
					i = j + 1;
				}

				name = stringBuffer.toString();
			}
		}
		return name;
	}

	public static String[] createCaptionsByPropertyIds(String[] propertyIds) {
		List<String> captions = new ArrayList<String>();
		for (String propertyId : propertyIds) {
			captions.add(StringUtils.createCaptionByPropertyId(propertyId));
		}
		return captions.toArray(new String[] {});
	}

	public static String day(Date date) {
		return toMonth(date, 0)[0];
	}

	public static String month(Date date) {
		return toMonth(date, 0)[1];
	}

	public static String year(Date date) {
		return toMonth(date, 0)[2];
	}

	public static String[] toMonth(Date date, int i) {

		DateFormat df = new SimpleDateFormat("dd");
		DateFormat mf = new SimpleDateFormat("M");
		DateFormat yf = new SimpleDateFormat("yyyy");

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + i);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new String[] { df.format(cal.getTime()), mf.format(cal.getTime()), yf.format(cal.getTime()) };
	}

}
