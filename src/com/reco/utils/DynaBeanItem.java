package com.reco.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

import com.vaadin.data.Property;
import com.vaadin.data.util.MethodProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.converter.Converter;;

@SuppressWarnings("serial")
public class DynaBeanItem extends PropertysetItem {

	public DynaBeanItem(DynaBean dynaBean) {
		this(dynaBean, getPropertiesDescriptor(dynaBean.getDynaClass()));
	}

	static Map<String, DynaProperty> getPropertiesDescriptor(DynaClass dynaClass) {

		Map<String, DynaProperty> model = new HashMap<String, DynaProperty>();
		for (DynaProperty prop : dynaClass.getDynaProperties()) {
			model.put(prop.getName(), prop);
		}
		return model;
	}

	public DynaBeanItem(DynaBean dynaBean, Map<String, DynaProperty> map) {

		for (String propertyId : map.keySet()) {
			if (!propertyId.equals("id"))
				addItemProperty(propertyId, createProperty(map.get(propertyId), dynaBean));
		}
	}

	private Property createProperty(final DynaProperty dp, final DynaBean dynaBean) {
		// return new AbstractProperty() {
		//
		// @Override
		// public Object getValue() {
		// return dynaBean.get(dp.getName());
		// }
		//
		// @Override
		// public void setValue(Object newValue) throws ReadOnlyException,
		// ConversionException {
		// if (isReadOnly())
		// throw new ReadOnlyException();
		// else {
		// dynaBean.set(dp.getName(), convertValue(newValue, getType()));
		// fireValueChange();
		// }
		// }
		//
		// @Override
		// public Class<?> getType() {
		// return dp.getType();
		// }
		// };
		try {
			Method getMethod = DynaBean.class.getDeclaredMethod("get", String.class);
			Method setMethod = DynaBean.class.getDeclaredMethod("set", String.class, Object.class);
			Class type = dp.getType();
			return new MethodProperty<Object>(type, dynaBean, getMethod, setMethod, new Object[] { dp.getName() },
					new Object[] { dp.getName(), "valor" }, 1);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Convert a value to the given type, using a constructor of the type that
	 * takes a single String parameter (toString() for the value) if necessary.
	 * 
	 * @param value
	 *            to convert
	 * @param type
	 *            type into which the value should be converted
	 * @return converted value
	 */
	static Object convertValue(Object value, Class<?> type) {
		if (null == value || type.isAssignableFrom(value.getClass())) {
			return value;
		}

		// convert using a string constructor
		try {
			// Gets the string constructor
			final Constructor<?> constr = type.getConstructor(new Class[] { String.class });

			// Create a new object from the string
			return constr.newInstance(new Object[] { value.toString() });

		} catch (final java.lang.Exception e) {

			throw new Converter.ConversionException(e);
		}
	}

}
