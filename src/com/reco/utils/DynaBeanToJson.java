package com.reco.utils;

import java.util.List;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaProperty;

public class DynaBeanToJson {

	public DynaBeanToJson() {

	}

	public String convertirDynaBeanAJson(DynaBean dynabean) {
		StringBuilder json = new StringBuilder("{");

		for (DynaProperty property : dynabean.getDynaClass().getDynaProperties()) {
			if (dynabean.get(property.getName()) instanceof String) {
				String valor = ((String) dynabean.get(property.getName())).replaceAll("(\'|\")", " ");
				json.append("\"" + property.getName() + "\":\"" + valor + "\",");
			} else {
				json.append("\"" + property.getName() + "\":\"" + dynabean.get(property.getName()) + "\",");
			}

		}
		json.deleteCharAt(json.length() - 1);
		json.append("}");

		return json.toString();
	}

	public String convertirListDynaBeanAJson(List<DynaBean> listDynabean) {
		StringBuilder json = new StringBuilder("[");

		for (DynaBean dynabean : listDynabean) {
			json.append(convertirDynaBeanAJson(dynabean)).append(",");
		}
		json.deleteCharAt(json.length() - 1);
		json.append("]");
		return json.toString();
	}
}
