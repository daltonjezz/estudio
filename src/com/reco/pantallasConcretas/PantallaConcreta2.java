package com.reco.pantallasConcretas;

import com.reco.app.ui.PantallaConcreta;
import com.reco.ui.utils.SiNoDialog;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

public class PantallaConcreta2 extends PantallaConcreta {

	private static final long serialVersionUID = 1L;

	private VerticalLayout contenido;

	public PantallaConcreta2() {
		super("Pantalla Concreta 2", null);
		initUI();
	}

	@Override
	public void initUI() {
		contenido = new VerticalLayout();
		contenido.addComponent(new Label("Contenido 1"));
		Button boton = new Button("Click me!");
		boton.addClickListener(new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				SiNoDialog dialog = new SiNoDialog("Alerta", "¿Esta seguro que desea publicar los debitos?",
						new SiNoDialog.Callback() {

							@Override
							public void onDialogResult(boolean resultIsYes) {
								if (resultIsYes) {
									Notification notification = new Notification("Presiono SI",
											Notification.Type.HUMANIZED_MESSAGE);
									notification.setDelayMsec(15);
									notification.show(Page.getCurrent());
								}
							}
						});

				getUI().addWindow(dialog);
			}
		});
		contenido.addComponent(boton);

		addComponent(contenido);
	}

}
