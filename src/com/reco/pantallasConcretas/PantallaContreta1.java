package com.reco.pantallasConcretas;

import com.reco.app.ui.PantallaConcreta;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class PantallaContreta1 extends PantallaConcreta {

	private static final long serialVersionUID = 1L;
	private VerticalLayout contenido;
	private TextField textfield;
	private Button boton;

	public PantallaContreta1() {
		super("Pantalla Concreta 1", null);
		initUI();
	}

	@Override
	public void initUI() {
		contenido = new VerticalLayout();
		contenido.addComponent(new Label("Contenido 1"));
		textfield = new TextField("Contenido 2");
		contenido.addComponent(textfield);
		boton = new Button("Click");
		boton.addClickListener(new Button.ClickListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				System.out.println("textfield:" + textfield.getValue());
			}
		});
		contenido.addComponent(boton);
		addComponent(contenido);
	}

}
