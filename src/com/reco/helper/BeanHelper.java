package com.reco.helper;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.vaadin.server.VaadinServlet;

public class BeanHelper {

	public static Object getBean(String nombreBean) {
		ServletContext servletContext = VaadinServlet.getCurrent().getServletContext();
		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		return applicationContext.getBean(nombreBean);
	}
}
