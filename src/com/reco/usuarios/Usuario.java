package com.reco.usuarios;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer id;
	String nombre;
	String contrasenia;

	private Usuario() {
	}

	public Usuario(String nombre, String contrasenia) {
		this();
		this.nombre = nombre;
		this.contrasenia = contrasenia;
	}
}
