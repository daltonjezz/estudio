package com.reco.usuarios;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.reco.dao.Dao;

@Repository
public class UsuariosDAO extends Dao {

	public Usuario getUsuario(Integer idUsuario) {
		return getCurrentSession().get(Usuario.class, idUsuario);
	}

	public Usuario obtenerPorNombre(String nombre) {
		Query query = getCurrentSession().createQuery("from Usuario u where u.nombre = ?");
		query.setString(0, nombre);
		return (Usuario) query.uniqueResult();
	}
}
